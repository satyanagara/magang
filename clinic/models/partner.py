from odoo import models,fields,api,_

class Doctor(models.Model):
    _inherit = 'res.users'

    doctor = fields.Boolean(string='is A Doctor?')

class Partner(models.Model):
    _inherit = 'res.partner'

    patient = fields.Boolean(string='is a Patient?')
    insurance = fields.Boolean(string='is a Insurance Company?')
