from odoo import models,fields,api,_

class Product(models.Model):
    _inherit = 'product.template'

    clinic_item = fields.Boolean(string='Clinic Item')